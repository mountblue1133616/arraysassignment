const map=(array,callback)=>{
    if (!array|| array.length===0 || !Array.isArray(array) ){
        console.log("Please provide a valid array");
        return;
      }    
      if (typeof callback !== "function") {
        console.log("Callback is not a function.Enter again");
        return;
      }

    const mapArray=[];
    for (let index = 0; index < array.length; index++) {
        mapArray.push(callback(array[index], index, array))
      }
    return mapArray;
}

module.exports=map;
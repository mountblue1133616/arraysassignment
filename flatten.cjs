const flatten=(array,depth=1)=> {
    if (!Array.isArray(array)) {
        console.log("Please enter an array");
        return;
      }
    let flatArray = [];
    for (let index = 0; index< array.length; index++) {
      if (Array.isArray(array[index]) && depth>0) {
        flatArray = flatArray.concat(flatten(array[index],depth-1));
      } else {
        if (array[index]!==undefined)
        {
        flatArray.push(array[index])
      }
      }
    }
    return flatArray;
  }

module.exports=flatten;
  
const items = [1, 2, 3, 4, 5, 5]; 
const reduce=require(`../reduce.cjs`)

const callback=(accumulator,currentValue)=>{
    return accumulator+ currentValue
}

console.log(reduce(items,callback,0))
console.log(reduce(items,callback,10))
console.log(reduce(items,callback,-10))
console.log(reduce(""))
console.log(reduce([1,2,3],""))
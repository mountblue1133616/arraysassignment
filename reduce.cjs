const reduce=(array,callback,initialValue)=>{
    if (!Array.isArray(array)) {
        console.log("Please enter an array");
        return;
      }
      
      if (typeof callback !== "function") {
        console.log("Callback is not a function.Enter again");
        return;
      }
    let accumulator;
    if (initialValue==undefined){
        accumulator=undefined
    }
    else {
        accumulator=initialValue
    }
    for (let index=0;index<array.length;index++){
        if(accumulator!==undefined){
            accumulator=callback(accumulator,array[index],index,array)
        }
        else {
            accumulator=array[index]
        }
    }
    return accumulator;

}

module.exports=reduce;
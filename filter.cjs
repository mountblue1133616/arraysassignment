const filter=(array,callback)=>{
    if (!Array.isArray(array)) {
        console.log("Please enter an array");
      }
      
      if (typeof callback !== "function") {
        console.log("Callback is not a function.Enter again");
      }

    const filterArray=[]
    for (let index = 0; index < array.length; index++) {
        if(callback(array[index], index, array)===true)
        filterArray.push(array[index])
      }
    return filterArray;
}

module.exports=filter;